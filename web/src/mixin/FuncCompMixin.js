// 功能组件混入对象

const FuncCompMixin = {
  props: {
    location: {
      type: String,
      default: ''
    }
  },
  created () {},
  destroyed () {

  },
  data () {
    return {

    }
  },
  methods: {

    /**
     * 初始化数据源
     * @param _staticCallback 数据源回调
     */
    initDatasource (_staticCallback) {
      _staticCallback()
    },

  },
  computed: {
    projectInfo () {
      if(this.$store.state.release.pageMetadata) {
        return this.$store.getters['release/getProjectInfo']
      }else {
        return this.$store.getters['designer/getProjectInfo']
      }
    },
    pageMetadata () {
      if(this.$store.state.release.pageMetadata) {
        return this.$store.getters['release/getPageMetadata']
      }else {
        return this.$store.getters['designer/getPageMetadata']
      }
    },
    component: function () {
      if(this.$store.state.release.pageMetadata) {
        return this.$store.getters['release/getLayoutItemById'](this.location).component
      }else {
        return this.$store.getters['designer/getLayoutItemById'](this.location).component
      }
    },

    /**
     * 是否显示布局块遮罩层
     * @returns {boolean}
     */
    shade () {
      if(this.$store.state.release.pageMetadata) {
        return false
      }else {
        return true;
      }
    },

    /**
     * 默认图片
     * @returns {string}
     */
    defaultImg () {
      return 'this.src="'+require('@/assets/image/default-img.png')+'";this.onerror=null'
    }
  }
};

export default FuncCompMixin
